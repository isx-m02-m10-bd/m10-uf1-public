## Exercici 1

Doneu data i hora actual del sistema en format dd/MM/aaaa-hh:mm:ss

## Exercici 2

Doneu hora actual en format hh:mm:ss


## Exercici 3

Determineu la durada de cada contracte fins avui en dies. S'ha de mostrar el
nom del treballador, la data del contracte i la durada.

## Exercici 4

Determineu la suma total de les durades de tots els contractes dels
treballadors en dies fins avui.

## Exercici 5

Determineu la diferència de temps treballat entre cadascun dels treballadors
amb el treballador més antic. S'ha de mostrar el nom del treballador, la data
del contracte i la diferència.

## Exercici 6

Calculeu el nombre de comandes fetes el desembre pels representants de vendes contractats el mes de febrer.

## Exercici 7

Llisteu el número de treballadors que s'han contractat per a cada mes de l'any. El llistat ha d'estar ordenat pel mes. Ha de tenir el següent format:

```
 mes_de_contractacio | nombre_de_contractes
---------------------+----------------------
                   1 |                    1
                   2 |                    1
                   3 |                    1
...
```
