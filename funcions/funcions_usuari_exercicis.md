# Funcions d'usuari


## Exercici 1

Llisteu l'identificador i el nom dels representants de vendes. Mostrar un camp
anomenat "result" que mostri 0 si la quota és inferior a les vendes, en cas
contrari ha de mostrar 1 a no ser que sigui director d'oficina, en aquest cas
ha de mostrar 2.

## Exercici 2

Llisteu tots els productes amb totes les seves dades afegint un nou camp
anomenat "div". El camp div ha de contenir el resultat de la divisió entre el
preu i les existències. En cas de divisió per zero, es canviarà el resultat a
0.

## Exercici 3

Afegeu una condició a la sentència de l'exercici anterior per tal de només
mostrar aquells productes que tinguin el valor del camp div menor a 1.

## Exercici 4

Donat l'identificador d'un client retorneu la importància del client, és a
dir, el percentatge dels imports de les comandes del client respecte al total
dels imports de les comandes.

## Exercici 5

Calculeu el que s'ha deixat de cobrar per a un producte determinat.
És a dir, la diferència que hi ha entre el que val i el que li hem cobrat al total de clients.

## Exercici 6

Creeu una funció que si li passem les columnes vendes i quota ens retorni una
columna amb el valor de vendes - quota.

## Exercici 7

ºFeu una funció que donat un identificador de representant de vendes retorni
l'identificador dels clients que té assignats amb el seu límit de crèdit.

## Exercici 8

Crear una funció promig_anual(venedor, any) que retorni el promig d'imports de
comandes del venedor durant aquell any.

## Exercici 9

Creeu una funció max_promig_anual(anyo) que retorni el màxim dels promitjos
anuals de tots els venedors. Useu la funció de l'exercici anterior.

## Exercici 10

Creeu una funció promig_anual_tots(anyo) que retorni el promig anual de cada
venedor durant l'any indicat. Useu funcions creades en els exercicis anteriors.

## Exercici 11

Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.

## Exercici 12

Funció anomenada maxim_mes a la que se li passa un any i retorna el mes en el
que hi ha hagut les màximes vendes (imports totals del mes).

## Exercici 13

1. Creeu una funció baixa_rep que doni de baixa el venedor que se li passa per
paràmetre i reassigni tots els seus clients al venedor que tingui menys clients
assignats (si hi ha empat, a qualsevol d'ells).


2. Com usarieu la funció per donar de baixa els 3 venedors que fa més temps que
no han fet cap venda?


## Exercici 14

Creeu una funció anomenada *n_clients* que donat un identificador d'un
representant de vendes ens retorni el nombre de clients que te assignats. Si
l'entrada és nul·la s'ha de retornar un valor nul. 


## Exercici 15

Creeu una funció anomenada *n_atesos* que donat un identificador d'un
representant de vendes ens retorni el nombre de clients diferents que ha atès.
Si l'entrada és nul·la s'ha de retornar un valor nul.


## Exercici 16

Creeu una funció anomenada *total_imports* que donat un identificador d'un
representant de vendes ens retorni la suma dels imports de les seves comandes.
Si l'entrada és nul·la s'ha de retornar un valor nul.


## Exercici 17

Creeu una funció anomenada "informe_rep" que ens retorni una taula amb
l'identificador del representant de vendes, el resultat de *n_clients*,
*n_atesos* i "total_imports*. Si a la funció se li passa un identificador de
representant de vendes només ha de retornar la informació relativa a aquest
representant de vendes. En cas de passar un valor nul a la funció, aquesta ha
de donar la informació de tots els representants de vendes.

## Exercici 18

Creeu una funció, i les funcions auxiliars convenients, que rebi l'identificador
d'un producte i retorni una taula amb les següents dades:

+ Identificador de producte i fabricant.
+ Nombre de representants de vendes que han venut aquest producte.
+ Nombre de clients que han comprat el producte.
+ Mitjana de l'import de les comandes d'aquest producte.
+ Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.

## Exercici 19

Creeu una funció que donada una oficina ens retorni una taula identificant els
productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.



## Exercici 20

Creeu les funcions necessàries per aconseguir el següent resultat:

+ Cridant *resum_client()* ha de retornar una taula amb els identificadors dels
clients, la suma de les seves compres, el nombre de comandes realitzades i el
nombre de representants de vendes que l'han atès.


+ Cridant *resum_client(num_clie)* ha de retornar una taula amb els
identificador dels representants de vendes i el nombre de comandes que ha
realitzat el client amb aquest representant de vendes


+ Cridant *resum_client(num_clie, num_empl)* ha de retornar una taula amb el
nombre de productes diferents que ha demanat el client especificat al
representant de vendes especificat i la mitja de l'import de les comandes que
ha realitzat el client especificat al representant de vendes especificat.


## Exercici 21

Creeu la funció *millor_venedor()* que retorni totes les dades del venedor que ha
venut més (major total d'imports) durant l'any en curs.

## Exercici 22

Calcular el descompte fet a un client concret, respecte totes les comandes del client.

Cal crear dos funcions auxiliars:

+ La primera obtindrà el total dels imports de les comandes d'un client determinat.

+ La segona serà una funció preu de "comanda abstracta" que necessitarà el
producte en qüestió i la quantitat de productes demanats.


