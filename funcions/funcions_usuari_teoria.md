# Funcions d'usuari

## CASE - WHEN - ELSE - END

Especifica una estructura condicional dins d'una sentència SQL.

**Exemples**

Llisteu els noms dels venedors i una columna que dessigni si són majors de 40
anys o no amb el text "Adult" o "Jove" respectivament

```
SELECT nom, 
       CASE 
       WHEN edat > 40 THEN 'Adult' 
       ELSE 'Jove'
       END
  FROM rep_vendes;
```

Llisteu els codis dels productes denotant si el preu és menor a 100 ('molt
barat'), entre 100 i 1000 ('barat'), entre 1000 i 3000 ('car') o més gran a
3000 ('molt car').

```
SELECT id_fabricant, id_producte, preu,
       CASE 
       WHEN preu < 100 THEN 'molt barat' 
       WHEN preu < 1000 THEN 'barat' 
       WHEN preu < 3000 THEN 'car' 
       ELSE 'molt car' 
       END 
  FROM productes;
```

Llisteu els productes i especifiqueu si són molt venuts (més de 2 comandes) o
no. Ho especificarem posant el text 'molt venut' o 'poc venut'.

```
SELECT id_fabricant, id_producte, 
       CASE 
       WHEN count(*) > 2 THEN 'molt venut' 
       ELSE 'poc venut' 
       END 
 FROM productes 
      JOIN comandes
      ON id_fabricant = fabricant AND id_producte = producte 
GROUP BY id_fabricant, id_producte;
```

Llisteu nom, edat i vendes dels venedors que compleixin el següent si tenen menys de 40 anys només llistarem els venedors que tinguin vendes superiors a 200000, si tenen 40 anys o més només llistarem els venedors que tinguin unes vendes superiors a 300000.

```
SELECT nom, edat, vendes 
  FROM rep_vendes 
 WHERE CASE 
       WHEN edat < 40 THEN vendes > 200000 
       ELSE vendes > 300000 
       END;
```

Establiu la quota a 0 als menors de 35 anys i pugeu un 5% a la resta

```
UPDATE rep_vendes 
   SET quota =
       CASE 
       WHEN edat < 35 THEN 0 
       ELSE quota * 1.05
       END;
```

## Funcions SQL

Es pot definir una funció que contingui un conjunt de sentències SQL.

Poden retornar:
* Sense retorn
* 1 valor
  * 1 valor simple
  * 1 valor compost:
    * paràmetres OUT(tipus o nom de taula o vista)
    * un tipus definit per l'usuari
    * el nom d'una taula o vista existent
* Conjunt de valors: 
  * SETOF *tipus*, on *tipus* és:
    * un tipus predefinit
    * un tipus definit per l'usuari
    * el nom d'una taula o vista existent
  * Una taula nova: RETURNS TABLE

### Sense retorn

Es retorna void.

**Exemples**

```
CREATE OR REPLACE FUNCTION puja_quota(INTEGER, NUMERIC) RETURNS void AS $$
     UPDATE rep_vendes
        SET quota = quota + $2
      WHERE num_empl = $1;
$$ LANGUAGE SQL;

CREATE FUNCTION esborra_joves() RETURNS void AS $$
     DELETE FROM rep_vendes
      WHERE edat < 40;
$$ LANGUAGE SQL;
```

### Retorn d'un valor simple

Retorna la primera fila de l'ultima consulta feta:

**Exemples:**

Retorna el número 1:

```
CREATE FUNCTION one() RETURNS INTEGER AS $$
     SELECT 1;
$$ LANGUAGE SQL;

SELECT one();
```

Funció que retorni el nom del primer venedor ordenats alfabèticament per nom:

```
CREATE FUNCTION nom_venedor1() RETURNS text AS $$
     SELECT nom 
       FROM rep_vendes
      ORDER BY nom;
$$ LANGUAGE SQL;

SELECT nom_venedor1();
```

Suma de dos nombres passats per paràmetre:

```
CREATE FUNCTION add_em(INTEGER, INTEGER) RETURNS INTEGER AS $$
     SELECT $1 + $2;
$$ LANGUAGE SQL;
```

O a partir de la versió 9.2 de PostgreSQL:

```
CREATE FUNCTION add_em(x INTEGER, y INTEGER) RETURNS INTEGER AS $$
     SELECT x + y;
$$ LANGUAGE SQL;

SELECT add_em(1,2);
```

Funció que retorni la suma de les vendes fetes pels venedors majors a una edat determinada:

```
CREATE FUNCTION sum_vendes_edat(INTEGER) RETURNS NUMERIC  AS $$
     SELECT SUM(vendes) 
       FROM rep_vendes 
      WHERE edat > $1;
$$ LANGUAGE SQL;

SELECT sum_vendes_edat(40);
SELECT sum_vendes_edat(60);
```

Enlloc de posar el tipus concret podem referenciar el tipus d'una columna d'una taula:

Funció que donat un venedor ens retorni les seves vendes

```
CREATE FUNCTION vendes_rep(SMALLINT) RETURNS NUMERIC(8,2) AS $$
     SELECT vendes 
       FROM rep_vendes 
      WHERE num_empl=$1; 
$$ LANGUAGE SQL;
```
    
Es podrien referenciar els tipus, així si canvien en la taula, la funció segueix sent correcta:

```
CREATE FUNCTION vendes_rep(rep_vendes.num_empl%TYPE) RETURNS rep_vendes.vendes%TYPE AS $$
     SELECT vendes 
       FROM rep_vendes 
      WHERE num_empl=$1; 
$$ LANGUAGE SQL;
    
SELECT vendes_rep(101::SMALLINT);
```

També podem cridar la funció passant-li el nom d'una columna dins un SELECT i
la funció s'executarà per a cadascun dels valors de la columna. Per exemple, si
volem veure les vendes dels directors d'oficina, mostrant la ciutat i les
vendes:

```
SELECT ciutat, vendes_rep(director)
  FROM oficines;
```

Si la consulta no és un SELECT tenim dues opcions. Afegir un SELECT auxiliar o
posar la clàusula RETURNING:

```
CREATE FUNCTION puja_quota(INTEGER, NUMERIC) RETURNS NUMERIC AS $$
     UPDATE rep_vendes
        SET quota = quota + $2
      WHERE num_empl = $1;
      
     SELECT 1.0;
$$ LANGUAGE SQL;
```

```
CREATE OR REPLACE FUNCTION puja_quota(INTEGER, NUMERIC) RETURNS NUMERIC AS $$
     UPDATE rep_vendes
        SET quota = quota + $2
      WHERE num_empl = $1;
        
     SELECT quota
       FROM rep_vendes
      WHERE num_empl = $1;
$$ LANGUAGE SQL;
```

```
CREATE OR REPLACE FUNCTION puja_quota(INTEGER, NUMERIC) RETURNS NUMERIC AS $$
     UPDATE rep_vendes
        SET quota = quota + $2
      WHERE num_empl = $1
  RETURNING quota; 
$$ LANGUAGE SQL;
```

Recordem que sempre tenim l'opció void que no retorna res si és l'opció més adient.

### Retorn d'un valor compost amb paràmetres OUT

Els paràmetres OUT són paràmetres de sortida, es crea una fila amb els valors OUT:

```
CREATE FUNCTION operations(IN x int, IN y int, OUT sum int, OUT prod int) AS $$
     SELECT x + y, x * y;
$$ LANGUAGE SQL;
```
Hi ha diverses maneres d'obtenir els resultats:

```
SELECT operations(3,7);
 operations 
------------
 (10,21)
(1 row)

SELECT (operations(3,7)).sum;
 sum 
-----
  10
(1 row)

SELECT (operations(3,7)).prod;
 prod 
------
   21
(1 row)

SELECT (operations(3,7)).*;
 sum | prod 
-----+------
  10 |   21
(1 row)

SELECT * FROM operations(3,7);
 sum | prod 
-----+------
  10 |   21
(1 row)
```

Donada una oficina, s'ha d'obtenir l'objectiu i les vendes:

```
CREATE FUNCTION objectiu_vendes(oficina SMALLINT, OUT objectiu NUMERIC, OUT vendes NUMERIC) AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE oficina = $1;
$$ LANGUAGE SQL;
```

### Retorn d'un valor compost d'un tipus definit per l'usuari

Si tenim un tipus compost definit podem usar aquest en la clausula RETURNS. Ens
retornarà el valor compost d'aquest tipus.

Donada una oficina, obtenir l'objectiu i les vendes:

```
CREATE TYPE objectiu_vendes_type AS (objectiu NUMERIC, vendes NUMERIC);

CREATE FUNCTION objectiu_vendes(oficina SMALLINT) RETURNS objectiu_vendes_type AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE oficina = $1;
$$ LANGUAGE SQL;
```

### Retorn d'un valor compost a partir del nom d'una taula o vista existent

Si tenim una taula/vista, podem posar el nom d'aquesta a RETURNS. Se'ns retornarà un valor compost amb la mateixa estructura de la taula/vista.

Donada una oficina, obtenir l'objectiu i les vendes:

```
CREATE TABLE objectiu_vendes_table (objectiu NUMERIC, vendes NUMERIC);

CREATE FUNCTION objectiu_vendes(oficina SMALLINT) RETURNS objectiu_vendes_table AS $$
    SELECT objectiu, vendes
      FROM oficines
     WHERE oficina = $1;
$$ LANGUAGE SQL;
```

Donada una oficina, obtenir l'objectiu i les vendes:

```
CREATE VIEW objectiu_vendes_view AS 
    SELECT objectiu, vendes 
      FROM oficines;

CREATE FUNCTION objectiu_vendes(oficina SMALLINT) RETURNS objectiu_vendes_view AS $$
    SELECT objectiu, vendes
      FROM oficines
     WHERE oficina = $1;
$$ LANGUAGE SQL;
```

Obteniu dels venedor joves, els menors de 40, el primer segons ordre alfabètic.

```
CREATE FUNCTION primer_venedor_jove() RETURNS rep_vendes AS $$
     SELECT *
       FROM rep_vendes
      WHERE edat < 40
      ORDER BY nom
      FETCH FIRST 1 ROWS ONLY;
$$ LANGUAGE SQL;
```

### Retorn d'un conjunt de files

Per retornar un conjunt de files s'usa la paraula clau *SETOF*.

#### Retorn d'un conjunt de valors d'un tipus predefinit

Codis d'oficina de la regió indicada:

```
CREATE FUNCTION oficines_regio(regio VARCHAR(10)) RETURNS SETOF SMALLINT AS $$
     SELECT oficina
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```

#### Retorn d'un conjunt de valors d'un tipus definit per l'usuari

Objectiu i vendes de les oficines d'una regió determinada:

```
CREATE TYPE objectiu_vendes_type AS (objectiu NUMERIC, vendes NUMERIC);

CREATE FUNCTION objectiu_vendes(regio VARCHAR) RETURNS SETOF objectiu_vendes_type AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```


#### Retorn d'un conjunt de valors a partir del nom d'una taula o vista existent

Objectiu i vendes de les oficines d'una regió determinada:

```
CREATE TABLE objectiu_vendes_table (objectiu NUMERIC, vendes NUMERIC);

CREATE FUNCTION objectiu_vendes(regio VARCHAR) RETURNS SETOF objectiu_vendes_table AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```

Objectiu i vendes de les oficines d'una regió determinada:

```
CREATE VIEW objectiu_vendes_view AS 
SELECT objectiu, vendes 
  FROM oficines;

CREATE FUNCTION objectiu_vendes(regio VARCHAR(10)) RETURNS SETOF objectiu_vendes_view AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```
Totes les dades de les oficines d'una regió determinada:

```
CREATE FUNCTION oficines_regio(regio VARCHAR) RETURNS SETOF oficines AS $$
     SELECT *
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```
Totes les dades dels venedors joves.

```
CREATE FUNCTION venedors_joves() RETURNS SETOF rep_vendes AS $$
     SELECT *
       FROM rep_vendes
      WHERE edat < 40;
$$ LANGUAGE SQL;
```

### Retorn d'una taula nova

Per tornar una o més files, es pot retornar una taula definida "al vol".

Objectiu i vendes de les oficines d'una regió determinada:

```
CREATE FUNCTION objectiu_vendes(regio VARCHAR(10)) RETURNS TABLE (objectiu NUMERIC, vendes NUMERIC) AS $$
     SELECT objectiu, vendes
       FROM oficines
      WHERE regio = $1;
$$ LANGUAGE SQL;
```

### Eficiència amb els NULLS

En algunes funcions és possible que un o més dels valors d'entrada puguin ser nuls, de manera que el resultat que retorna la funció, com ja era de preveure sense necessitat d'haver executat la funció, també és NUL.

Si ja ho sabem, podem fer que en el cas de que hi hagi al menys un NUL d'entrada la funció retorni un NUL. D'aquesta manera ens estalviem executar el codi de la funció. Per indicar aquest comportament haurem d'afegir al final de la sintaxi de SQL `RETURNS SETOF NULL ON NULL INPUT` de manera que la darrera línia de la funció quedarà:

```
...

$$ LANGUAGE sql RETURNS SETOF NULL ON NULL INPUT;
```

És important tenir en compte que si es retorna una taula, no sortirà cap valor NUL, sinó una taula amb 0 files. Si volem que explícitament es vegin els NUL's podem pensar a fer servir ls paràmetres OUT.



## S'ha entès bé?

**Pregunta:**

Suposem que volem retornar tots els venedors que han fet una comanda d'un
producte determinat. Ho aconseguiré amb la següent construcció?

```
CREATE TABLE venedors_producte(empl smallint);


CREATE FUNCTION get_venedors_producte(fab CHAR, prod CHAR) RETURNS venedors_producte AS $$
	SELECT rep
	  FROM comandes
	 WHERE fabricant = fab
       AND producte = prod;
$$ LANGUAGE SQL;
```


