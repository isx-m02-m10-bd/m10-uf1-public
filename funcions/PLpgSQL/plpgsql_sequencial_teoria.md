# PL/pgSQL: Estructura sequencial

## Introducció

PL/pgSQL és un llenguatge procedural utilitzat a un SGBD PostgreSQL.

Algunes de les principals característiques d'aquest llenguatge:

+ Es pot utilitzar per crear funcions, procedures i triggers.
+ Afegeix estructures de control al llenguatge SQL

D'altra banda, pensem que una funció amb SQL només retorna informació relativa a la darrera consulta, de manera que si tenim una aplicació client que farà executarà un acció o una altre en funció del resultat d'una consulta haurem de connectar-nos diferents vegades al servidor.

+ Elimina connexions extres entre el client i el servidor.


## Estructura


L'estructura bàsica d'una funció PLpgSQL, com les funcions SQL, bàsicament té
el següent format:

```
CREATE FUNCTION function_name(arguments) RETURNS type AS 
	'function body text'
LANGUAGE plpgsql;
```

Com que alguna de les instruccions del body podria contenir una cometa, per
diferenciar-les de les cometes del codi s'haurien d'escapar. Per evitar aquest
problema Postgresql proporciona la notació del doble dòlar $$


```
CREATE FUNCTION function_name(arguments) RETURNS type AS $$
	function body text
$$ LANGUAGE plpgsql;
```


Estructura típica:

```
CREATE FUNCTION function_name(arguments) RETURNS type AS $$
DECLARE
	declaration;
	[...]
BEGIN
	statement;
	[...]
END;
$$ LANGUAGE 'plpgsql';
```

Es poden fer subblocs.

Exemples:
```
CREATE FUNCTION hello_world() RETURNS text AS $$
BEGIN
	RETURN 'Hello world!';
END;
$$ LANGUAGE plpgsql;
```
## Comentaris

```
-- This will be interpreted as a single-line comment.

/*
 *  This is a
 *  block
 *  comment.
 */
```

## Declaracions

Les variables usades en un bloc han d'estar declarades en la secció DECLARE.

Sintaxi:
```
name [ CONSTANT ] type [ NOT NULL ] [ { DEFAULT | := } expression ];
```

CONSTANT: el valor no es pot canviar
NOT NULL: si intentem assignar NULL a la variable donarà error
:= o DEFAULT: es posa un valor per defecte

Exemples:
```
CREATE FUNCTION assignment_example () RETURNS text AS $$
DECLARE
	user_id integer;
	quantity numeric(5);
	url varchar := 'http://mysite.com';

	-- Declare a constant integer with a
	-- default value of 5.
	five CONSTANT integer := 5;

	-- Declare an integer with a default
	-- value of 100 that cannot be NULL.
	ten integer NOT NULL := 10;

	-- Declare a character with
	-- a default value of "a".
	letter char DEFAULT 'a';

BEGIN
	RETURN letter;
END;
$$ LANGUAGE 'plpgsql';
```
```
CREATE FUNCTION hello_var() RETURNS text AS $$
DECLARE
	name text := 'Mary'; 
BEGIN
	RETURN 'Hello ' || name || '!';
END;
$$ LANGUAGE plpgsql;
```

## Assignació

Operador d'assignació: **:=**

Exemples:
```
CREATE FUNCTION hello_var() RETURNS text AS $$
DECLARE
	name text := 'Mary';
BEGIN
	name := 'John';
	name := 'Mr. ' || name;
	RETURN 'Hello ' || name || '!';
END;
$$ LANGUAGE plpgsql;
```

```
CREATE FUNCTION two_plus_two() RETURNS int AS $$
DECLARE
	n1 int;
	n2 int;
	sum int;
BEGIN
	n1 := 2;
	n2 := 2;
	sum := n1 + n2;
	RETURN sum;
END;
$$ LANGUAGE plpgsql;
```

SELECT INTO: serveix per posar el resultat d'una consulta en una variable. La
consulta només ha de retornar una fila.

```
SELECT columns_selection INTO [STRICT] target FROM ...;
```

STRICT: si es posa, la consulta ha de retornar només una fila. Si no es posa s'agafarà el resultat de la primera fila o NULL si no es retorna cap fila.

Exemples:
```
CREATE FUNCTION obtenir_id_client(emp text) RETURNS integer AS $$
DECLARE
	id_client integer;
BEGIN
	SELECT num_clie 
	  INTO id_client
	  FROM clients
	 WHERE empresa = emp;
	
	RETURN id_client;
END;
$$ LANGUAGE 'plpgsql';
```

```
CREATE FUNCTION obtenir_id_limit_client(emp text) RETURNS text AS $$
DECLARE
	id_client integer;
	credit numeric;
BEGIN
	SELECT num_clie, limit_credit
	  INTO id_client, credit
	  FROM clients
	 WHERE empresa = emp;

	RETURN id_client || ': ' || credit;
END;
$$ LANGUAGE 'plpgsql';
```

FOUND: variable booleana que ens determinarà si s'ha assignat o no algun valor a la variable.

Si només ens retorna un valor, no cal fer servir SELECT INTO, ho podem fer amb
l'operador d'assignació:

```
CREATE FUNCTION obtenir_id_client(emp text) RETURNS integer AS $$
DECLARE
	id_client integer;
BEGIN
	id_client := (SELECT num_clie
	                FROM clients
	               WHERE empresa = emp);
	RETURN id_client;
END;
$$ LANGUAGE 'plpgsql';
```

## Funcions amb paràmetres

Els paràmetres de les funcions són identificats amb $1,$2.... Podem posar àlies
a aquests identificadors per fer que el codi sigui més llegible. A partir de la
versió 8.0 de PostgreSQL podem posar aquest àlies en els paràmetres:

```
CREATE FUNCTION sales_tax(subtotal real) RETURNS real AS $$
BEGIN
	RETURN subtotal * 0.06;
END;
$$ LANGUAGE plpgsql;
```

Abans d'aquesta versió s'havia d'usar ALIAS FOR:

```
CREATE FUNCTION sales_tax(real) RETURNS real AS $$
DECLARE
    subtotal ALIAS FOR $1;
BEGIN
    RETURN subtotal * 0.06;
END;
$$ LANGUAGE plpgsql;
```

**Tipus**

%TYPE: tipus d'una columna d'una taula determinada. Per exemple: *clientes.empresa%TYPE*

%ROWTYPE: tipus de la fila d'una taula. Per exemple: *clientes%ROWTYPE*. Després podem accedir als camps posant *variable.nom_camp*.

RECORD: una fila qualsevol. És un tipus que no té estructura definida.
Adquireix l'estructura del tipus que se li assigna. 

## Retorns

Retorna una variable:
```
RETURN var;
```

Afegir al retorn el resultat d'una consulta:
```
RETURN QUERY consulta;
...
RETURN;
```

Exemple:
```
CREATE FUNCTION comandes_2_mesos_1(mes1 int, mes2 int) RETURNS SETOF int AS $$ 
BEGIN
	RETURN QUERY SELECT num_comanda
	               FROM comandes
	              WHERE DATE_PART('month', data) = mes1
	                 OR DATE_PART('month', data) = mes2;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```
```
CREATE FUNCTION comandes_2_mesos_2(mes1 int, mes2 int) RETURNS SETOF int AS $$ 
BEGIN
	RETURN QUERY SELECT num_comanda
	               FROM comandes
	              WHERE DATE_PART('month', data) = mes1;
	RETURN QUERY SELECT num_comanda
	               FROM comandes
	              WHERE DATE_PART('month', data) = mes2;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```

## OBSERVACIONS

+ RETURN NEXT serveix per anar afegint valors al retorn final. El veurem quan estudiem bucles.

+ ALIAS no només serveix per a rebatejar els paràmetres de la funció, sinó per qualsevol variable. Un ús molt habitual és per les variables especials NEW i OLD que es fan servir als triggers.

