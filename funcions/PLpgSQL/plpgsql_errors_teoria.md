# Captura d'errors


## Problema

Comencem amb una funció molt senzilla:

```
CREATE FUNCTION divisio(num1 numeric, num2 numeric) RETURNS numeric AS $$
BEGIN	
		RETURN num1 / num2;
END
$$ LANGUAGE plpgsql;
```

```
SELECT divisio(2,0);

ERROR:  division by zero
CONTEXT:  PL/pgSQL function divisio(numeric,numeric) line 4 at RETURN
```

Quan existeix la possibilitat de que hi hagi errors a una funció, haurem de decidir si els volem tractar o no.
Tenim dues opcions:

+ No tractar-los. Una bona raó pot ser considerar que estem amagant informació a l'usuari que executa la funció o a una segona funció que crida la primera. 

+ Tractar-los. En aquest cas farem servir el bloc EXCEPTION.

## Sintaxi: 

Si volem tractar un error a PLpgSQL podem afegir dintre del clàssic bloc de
PLpgSQL un subbloc anomenat EXCEPTION.

D'aquesta manera si hi ha un error a una instrucció del bloc saltarà a
l'apartat EXCEPTION i si coincideix el tipus d'error, farà el que digui el codi.

```
[ DECLARE
	declarations ]
BEGIN
	statements
EXCEPTION
	WHEN condition [ OR condition ... ] THEN
		handler_statements
	[ WHEN condition [ OR condition ... ] THEN
		handler_statements
	... ]
END;
```

## Exemples:

En general a l'exemple amb que introduim aquest tema no hauríem de tractar
l'error, ja que no és responsabilitat de la funció comprovar que el denominador
no és 0. Però podria haver alguna situació molt especial en que sí que fos
necessari ...


```
CREATE FUNCTION divisio(num1 numeric, num2 numeric) RETURNS numeric AS $$
BEGIN
	RETURN num1 / num2;
EXCEPTION
	WHEN division_by_zero THEN
		RAISE NOTICE 'Collons no pots dividir per 0, inventant resultat:';
		RETURN -8888888888888888;
END
$$ LANGUAGE plpgsql;
```

D'on hem tret el nom o codi d'aquest error? [D'aquest
enllaç](https://www.postgresql.org/docs/13/errcodes-appendix.html)


Insistim en que és molt important decidir quan una funció ha de gestionar els
errors sense amagar informació.


Un altre exemple:

```
-- Creem una taula
CREATE TABLE db ( PRIMARY KEY(a), a INT, b TEXT);

-- Creem una funció 
CREATE OR REPLACE FUNCTION merge_db(key INT, data TEXT) RETURNS VOID AS $$
BEGIN
	LOOP
		-- primer intentem actualitzar la clau
		UPDATE db SET b = data
		 WHERE a = key;
		
		-- Si l'ordre anterior ha actualitzat alguna fila sortim de la funció 
		IF found THEN
			RETURN;
		END IF;

		-- dormim 5 segons per donar temps a que mentre intentem inserir la key
		-- des d'una altra terminal s'actualitzi la mateixa key (de manera concurrent)
		-- i salti una errada -> unique-key
		PERFORM pg_sleep(5);

		-- Ho fem dins d'un altre block

		BEGIN
			INSERT INTO db(a,b)
			VALUES (key, data);
			RETURN;

--		EXCEPTION WHEN unique_violation THEN
	
			-- Només mostrem un missatge, però no fem res més. 
			-- Continuarà el bucle i s'intentarà fer l'UPDATE que ara sí que existirà
--			RAISE NOTICE 'no fem res, de manera que tornem al bucle i s''intenta l''UPDATE un altre cop';
		END;

	END LOOP;
END;
$$ LANGUAGE plpgsql;
```

+ Primer amb les dues línies comentades per veure com salta l'error.

	Des d'una terminal:

	```
	SELECT merge_db(1, 'david');
	```

	Des d'una altra (abans de 5 segons) executo:

	```
	SELECT merge_db(1, 'dennis');
	```

	A la 2a terminal es mostrarà:

	```
	ERROR:  duplicate key value violates unique constraint "db_pkey"
	DETAIL:  Key (a)=(1) already exists.
	CONTEXT:  SQL statement "INSERT INTO db(a,b)
				VALUES (key, data)"
	PL/pgSQL function merge_db(integer,text) line 20 at SQL statement
	```

+ REPETIM però ara descomentem les línies EXCEPTION i RAISE per gestionar
  nosaltres l'errada.

	Recordem però d'eliminar el que hi ha a la taula db:

	```
	DELETE FROM db;
	```

	Tornem a fer el mateix d'abans i ara el missatge serà diferent (el
gestionem nosaltres) i la clau 1 quedarà com a dennis si ha estat el 2on:

	```
	NOTICE:  no fem res, de manera que tornem al bucle i s'intenta l'UPDATE un altre cop
	```

## La instrucció RAISE. RAISE NOTICE vs RAISE EXCEPTION:

RAISE llença missatges i errors.

Hi ha diferents tipus de nivell de missatges (DEBUG, LOG, INFO, NOTICE, WARNING)

Si llancem un missatge d'algun d'aquells nivells, la funció no acaba amb error.

Però si fem servir RAISE EXCEPTION (o el que és el mateix: RAISE sense nivell)
es considera que la funció acaba amb error.

A la següent funció, intercanvia el comentari entre les dues línies RAISE per
veure les diferències.

```
CREATE OR REPLACE FUNCTION div0() RETURNS void AS $$
DECLARE
	n int;
BEGIN
	RAISE NOTICE 'Inici BEGIN 1';
	BEGIN
		RAISE NOTICE 'Inici BEGIN 2';
		n := 3 / 0;
		RAISE NOTICE 'Fi BEGIN 2';
	EXCEPTION
		WHEN division_by_zero THEN
			RAISE NOTICE 'divisió entre 0';
			-- RAISE EXCEPTION 'divisió entre 0';
	END;
	RAISE NOTICE 'Fi BEGIN 1';
END;
$$ LANGUAGE 'plpgsql';
```

```
SELECT div0();
NOTICE:  Inici BEGIN 1
NOTICE:  Inici BEGIN 2
NOTICE:  divisió entre 0
NOTICE:  Fi BEGIN 1
 div0 
------
 
(1 row)
```

```
SELECT div0();
NOTICE:  Inici BEGIN 1
NOTICE:  Inici BEGIN 2
ERROR:  divisió entre 0
CONTEXT:  PL/pgSQL function div0() line 13 at RAISE
```



## Informació de l'error

Podem extreure informació de l'error fent servir la instrucció:

```
GET STACKED DIAGNOSTICS variable = item [ , ... ];
```

A on item pot ser un dels valors [d'aquesta taula](https://www.postgresql.org/docs/13/plpgsql-control-structures.html#PLPGSQL-EXCEPTION-DIAGNOSTICS-VALUES)

Posem un exemple:

```
CREATE OR REPLACE FUNCTION div0() RETURNS void AS $$   
DECLARE
	n int;
	text_var1 text;
	text_var2 text;
	text_var3 text;
BEGIN
	RAISE NOTICE 'Inici BEGIN 1';
	BEGIN
		RAISE NOTICE 'Inici BEGIN 2';
		n := 3 / 0;
		RAISE NOTICE 'Fi BEGIN 2';
	EXCEPTION
		WHEN OTHERS THEN
			GET STACKED DIAGNOSTICS text_var1 = MESSAGE_TEXT,
									text_var2 = RETURNED_SQLSTATE,
									text_var3 = PG_EXCEPTION_CONTEXT;
			RAISE  'El missatge diu: %, el codi d''error és %, 
					mentre que l''ordre que ha petat és: % ', text_var1, text_var2, text_var3;
	END;
	RAISE NOTICE 'Fi BEGIN 1';
END;
$$ LANGUAGE 'plpgsql';
```

Extret [d'aquest link](https://www.postgresql.org/docs/13/plpgsql-control-structures.html#PLPGSQL-ERROR-TRAPPING)


## Els blocs BEGIN / END de PLpgSQL i les transaccions

Notem que els blocs de les funcions PLpgSQL BEGIN / END no són transaccions,
però sí que és habitual que una funció PLpgSQL formi part d'alguna operació que
sí que sigui una transacció.

I ja sabem que si una instrucció d'una transacció, que podria ser la crida a
aquesta funció, falla, la transacció fa un ROLLBACK.
