# PLpgSQL - Estructura repetitiva


## Exercici 1

Funció spread(text) que converteix el text d'entrada en un altre, passat a
majúscules, i amb un espai en blanc afegit després de cada caràcter de la
cadena original. 

Així "Hola, que tal?" donaria com a sortida "H O L A ,   Q U E   T A L ?".


## Exercici 2

Crea una funció que donat l'identificador d'un representant de vendes calculi la seva comissió. La comissió de cada representant de vendes es calcula de la següent manera:

+ La comissió d'un representant de vendes es composa de la comissions que cada representant de vendes obté de cada un dels encàrrecs que ha atès.

+ Per cada encàrrec la comissió és un percentatge de l'import sense descompte de l'encàrrec.

+ L'import sense descompte de l'encàrrec és el resultat de multiplicar el preu del producte per la quantitat de producte de l'encàrrec.

+ Si l'import sense descompte de l'encàrrec és superior o igual a 1500 el percentatge de comissió és 15%.

+ Si l'import sense descompte de l'encàrrec és inferior a 1500 però superior o igual a 500 el percentatge de comissió és del 10%.

+ Si l'import sense descompte de l'encàrrec és inferior a 500 el percentatge de comissió és 5%.

## Exercici 3

Volem generar llistats de venedors ordenats pels màxims períodes entre
comandes consecutives de cada venedor. 

Per aquest motiu necessitem una funció que donat un identificador de
representant de vendes retorni l'interval de temps més gran que hi ha hagut
entre dos comandes consecutives d'aquest representant de vendes.



## Exercici 4

Crea una funció que donat un identificador de client mostri per pantalla la seva factura. 

+ La factura ha de contenir tots els productes que ha demanat el client amb la data en que els ha demanat.

+ En cas de tenir descompte s'ha de calcular el descompte aplicat a cada comanda.

+ S'ha de calcular l'IVA i el total.

+ Crea les funcions necessàries per crear un codi modular.

La factura ha de tenir el següent aspecte:

```
Client (id): Ace International (2107)
=====================================
Data: 10/5/2012

Descripció del producte (id)  Data de la comanda  Quantitat  Preu Unitari  Descompte         Import
---------------------------------------------------------------------------------------------------
Manivela (bic, 41003)                   1/4/2012         10         54.00                    540.00
Perno Riostra (imm,887p)               12/4/2012         10         10.00        10%          90.00
Montador (aci,4100z)                   25/5/2012          1       1000.00        50%         500.00
===================================================================================================
                                                                           Base:            1130.00
                                                                       IVA(18%):             203.40
                                                                          Total:            1333.40
===================================================================================================
```


## Exercici 5

Crea una funció que, donat un client, un producte i una quantitat, retorni el
descompte que se li podria aplicar a la comanda.

+ En general els clients gaudeixen d'un descompte del 10% si el preu de la
comanda és superior a 1000.

+ També gaudeixen d'un descompte del 20% en cas que el preu de la comanda
sigui superior a 5000.

+ A més a més gaudeixen d'un descompte del 40% en cas que el preu de la
comanda sigui superior a 10000.

+ A les comandes de 2 o més productes del fabricant amb identificador imm i un
preu unitari superior a 500 tenen un descompte del 35%.  

+ Si el client ha acumulat un import superior a 20000 amb les comandes dels
últims 2 mesos aquest client és un client VIP.

+ Els clients VIP gaudeixen d'un descompte del 30% en comandes amb un preu
superior a 2000, en comandes amb preus inferiors el descompte serà del 20%.

+ Sempre s'aplicarà el descompte més beneficiós pel client.

