# PLpgSQL - Estructura seqüencial

## Exercici 1

Escriu una funció plpgsql que rebi com a paràmetre el codi d'un fabricant i
retorni el nombre de comandes de productes d'aquell fabricant.

## Exercici 2

Escriu una funció plpgsql que rebi com a paràmetre el codi d'un fabricant i
retorni el codi de producte del producte amb més unitats venudes d'aquell
fabricant.

## Exercici 3

(FU-4) Crea una funció que rebi l'identificador d'un client i que retorni la
importància del client, és a dir, el percentatge dels imports de les comandes
del client respecte al total dels imports de les comandes.

## Exercici 4

(FU-11) Feu una funció que retorni tots els codis dels clients que no hagin
comprat res durant el mes introduït.

## Exercici 5

(FU-18) Creeu una funció, i les funcions auxiliars convenients, perquè donat
l'identificador d'un producte s'obtingui una taula amb les següents dades:

+ Identificador de producte i fabricant.

+ Nombre de representants de vendes que han venut aquest producte.

+ Nombre de clients que han comprat el producte.

+ Mitjana de l'import de les comandes d'aquest producte.

+ Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola
  comanda.

## Exercici 6

(FU-19) Creeu una funció que donada una oficina ens retorni una taula
identificant els productes i mostrant la quantitat d'aquest producte que s'ha
venut a l'oficina.

## Exercici 7

(average) Calcula la mitjana aritmètica de tres nombres reals.

## Exercici 8

(hms2s) A partir d'un nombre enter d’hores, un nombre enter de minuts i un
nombre enter de segons, escriu el nombre de segons equivalents.

## Exercici 9

Donat un número de comanda i l'IVA a aplicar, calcula el preu de la comanda amb
l'IVA aplicat.

## Exercici 10

Donat un codi de fabricant, retorna els codis dels productes d'aquest
fabricant.



> On (FU-XX) = ( Exercici XX de la llista d'exercicis de Funcions d'Usuari )
