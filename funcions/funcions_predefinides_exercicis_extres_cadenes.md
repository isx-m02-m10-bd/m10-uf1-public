# EXTRES funcions predefinides cadena

> Heu de fer els següents exercicis amb una cadena d'exemple:

## Exercici 1 (reverse)

Donada una cadena, crea una altra cadena amb els caràcters de la cadena donada invertits.

## Exercici 2. (removeSpaces) 

Donada una cadena, crea una altra cadena igual a la primera sense espais en blanc.

## Exercici 3. (withoutString)

Donades dues cadenes, crea una nova cadena igual a la primera sense l'aparició de la segona. Si apareix més d'un cop, només es suprimirà la primera aparició.

## Exercici 4. (toUpper) 

Donada una cadena, crea una segona cadena amb els caràcters que estaven en minúscula passats a majúscula.

## Exercici 5. (dni2nif) 

Donada una cadena amb el número d'un DNI, genera una cadena amb el NIF.

El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el
residu de la divisió entera del DNI entre 23.

Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 ->T,
1->R, 2->W, etc.

Exemple: Al DNI  37721039 li correspon el NIF 37721039G

